/*global angular, localStorage*/

angular.module('settingsApp').factory('PrefsService', function () {
    'use strict';

    var service = {};

    service.put = function (key, value) {
        localStorage.setItem(key, value);
    };

    service.clear = function () {
        localStorage.clear();
    };

    return service;
});