/*global angular*/

angular.module('settingsApp').controller('SettingsCtrl', function ($scope, PrefsService) {
    'use strict';

    $scope.model = {
        minimized: false,
        launch: true
    };

    $scope.initSettings = function () {
        PrefsService.clear();
    };

    $scope.save = function (key, value) {
        $scope.model[key] = value;
        PrefsService.put(key, value);
    };
});