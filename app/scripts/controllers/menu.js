/*global angular*/

angular.module('settingsApp').controller('MenuCtrl', function ($scope, $state) {
    'use strict';

    $scope.isCurrentState = function (state) {
        return $state.is(state);
    };
});