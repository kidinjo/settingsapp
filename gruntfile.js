/*global module, require*/
module.exports = function (grunt) {
    'use strict';

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            tmp: ['.tmp'],
            distDirectory: ['dist']
        },

        jslint: {
            all: {
                src: ['app/**/*.js'],
                options: {
                    edition: 'latest',
                    failOnError: false
                }
            }
        },

        sass: {
            all: {
                files: [{
                    expand: true,
                    cwd: 'app/styles',
                    src: ['*.scss'],
                    dest: '.tmp/styles',
                    ext: '.css'
                }]
            }
        },

        bower_concat: {
            all: {
                dest: 'dist/scripts/vendor.js',
                cssDest: '.tmp/styles/vendor.css'
            }
        },

        concat: {
            options: {
                separator: '\n'
            },
            app: {
                src: 'app/**/*.js',
                dest: 'dist' + '/scripts/app.js'
            },
            styles: {
                src: ['.tmp/styles/*.css'],
                dest: 'dist' + '/styles/style.css'
            }
        },

        watch: {
            scripts: {
                files: ['app/**/*.js'],
                tasks: ['newer:jslint', 'concat:app']
            },
            styles: {
                files: ['app/**/*.scss'],
                tasks: ['newer:sass', 'concat:styles']
            },
            html: {
                files: ['app/**/*.html'],
                tasks: ['newer:copy:html']
            }
        },

        copy: {
            html: {
                files: [
                    {
                        expand: true,
                        cwd: 'app/',
                        src: ['**/*.html'],
                        dest: 'dist'
                    }
                ]
            },
            resources: {
                files: [
                    {
                        expand: true,
                        cwd: 'resources/',
                        src: ['**/**'],
                        dest: 'dist'
                    }
                ]
            }
        },

        connect: {
            server: {
                options: {
                    port: 9001,
                    base: 'dist'
                }
            }
        }
    });

    grunt.registerTask('default', ['clean', 'sass', 'jslint', 'bower_concat', 'concat', 'copy']);
    grunt.registerTask('serve', ['connect', 'watch']);
};