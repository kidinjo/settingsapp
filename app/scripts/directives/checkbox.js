/*global angular*/
/*jslint unparam:true*/
angular.module('settingsApp').directive('componentCheckbox', function ($timeout) {
    'use strict';
    return {
        restrict: 'E',
        require: ['?ngModel'],
        replace: true,
        scope: {
            options: '@',
            onSave: '&'
        },
        template: '<div class="component-checkbox">' +
            '<div class="checkbox" ng-click="toggle()">' +
            '<i class="icon ion-checkmark" ng-class="{active: value}"></i>' +
            '</div>' +
            '<div class="selection" ng-show="isSelected">' +
            '<div class="controls cancel" ng-click="cancel()">Cancel</div>' +
            '<div class="controls save" ng-click="save()">Save</div></div>' +
            '<div class="selection">' +
            '<div class="controls saved" ng-show="saved">Saved</div>' +
            '</div>' +
            '</div>',
        link: function ($scope, $element, $attrs, controllers) {
            var ngModelCtrl;
            $scope.isSelected = false;

            ngModelCtrl = controllers[0];

            if (ngModelCtrl) {
                ngModelCtrl.$formatters.push(function (modelValue) {
                    $scope.value = modelValue || '';
                });
            } else {
                $scope.value = false;
            }

            $scope.toggle = function () {
                $scope.value = !$scope.value;

                if ($scope.options) {
                    $scope.isSelected = !$scope.isSelected;
                }

                if (ngModelCtrl) {
                    ngModelCtrl.$setViewValue($scope.value);
                }
            };

            $scope.save = function () {
                $scope.onSave();
                $scope.isSelected = !$scope.isSelected;
                $scope.saved = true;

                $timeout(function () {
                    $scope.saved = false;
                }, 1000);
            };

            $scope.cancel = function () {
                $scope.isSelected = false;
                $scope.value = !$scope.value;
            };
        }
    };
});