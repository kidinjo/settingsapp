/*global angular*/
/*jslint unparam:true*/

angular.module('settingsApp').config(function ($locationProvider, $urlRouterProvider) {
    'use strict';
    $urlRouterProvider.otherwise('/settings');
});

angular.module('settingsApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('main', {
            templateUrl: 'views/menu.html',
            controller: 'MenuCtrl'
        })
        .state('main.settings', {
            url: '/settings',
            views: {
                'right-view': {
                    templateUrl: 'views/settings.html',
                    controller: 'SettingsCtrl'
                }
            }
        })
        .state('main.profile', {
            url: '/profile',
            views: {
                'right-view': {
                    templateUrl: 'views/unknown.html'
                }
            }
        })
        .state('main.account', {
            url: '/account',
            views: {
                'right-view': {
                    templateUrl: 'views/unknown.html'
                }
            }
        })
        .state('main.logout', {
            url: '/logout',
            views: {
                'right-view': {
                    templateUrl: 'views/unknown.html'
                }
            }
        });
});